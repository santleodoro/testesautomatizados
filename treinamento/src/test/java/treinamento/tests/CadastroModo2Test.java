package treinamento.tests;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import treinamento.pages.CadastroPageObject;
import treinamento.util.LeitorProperties;

/**
 * Classe respons�vel por executar os tests nas telas da aplica��o.
 * 
 * Aqui os testes est�o sendo chamados com um @Test para cada intera��o, prefiro
 * dessa forma, pois se houver erro de elemnto n�o encontrado em algum campo,
 * ser� poss�vel saber qual campo est� com problema.
 * 
 * @author Sandra Rodrigues
 *
 */
// Anota��o para informar que os testes devem ser chamados pela ordem dos nomes
// dos m�todos
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CadastroModo2Test {

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	// Classe respons�vel por conter os testes da p�gina de cadastro
	CadastroPageObject cadastroPageObject = new CadastroPageObject();

	// Para que a suite entenda que o m�todo deve ser chamado ele tem que ser
	// anotado com @Test assim como JUnit, sem essa anota��o esse m�todo n�o ser�
	// chamado.
	@Test
	public void A001AcessarMenuNovoUsuario() {
		cadastroPageObject.acessarMenuNovoUsario();
	}

	@Test
	public void A002InformarNome() {
		cadastroPageObject.informarNome(properties.getNome());
	}

	@Test
	public void A003InformarEmail() {
		cadastroPageObject.informarEmail(properties.getEmail());
	}

	@Test
	public void A004InformarSenha() {
		cadastroPageObject.informarSenha(properties.getSenha());
	}

	@Test
	public void A005Cadastrar() {
		cadastroPageObject.clicarBotaoCadastrar();
	}

	// Se n�o houver valida��es, estamos apenas criando um rob� que executa os
	// passos do usu�rio, por isso � necess�rio criar os asserts, s�o eles que
	// ir�o validar se a informa��o encontrada est� conforme a esperada.
	@Test
	public void A006ValidarCadastro() {
		cadastroPageObject.isUsuarioCadastrado();
	}

}
