package treinamento.tests;

import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import treinamento.pages.LoginPageObject;
import treinamento.util.LeitorProperties;
import treinamento.util.ScreenshotUtil;

/**
 * Classe respons�vel por executar os tests nas telas da aplica��o.
 * 
 * Aqui os testes est�o sendo chamados com um @Test para cada intera��o, prefiro
 * dessa forma, pois se houver erro de elemnto n�o encontrado em algum campo,
 * ser� poss�vel saber qual campo est� com problema.
 * 
 * @author Sandra Rodrigues
 *
 */
// Anota��o para informar que os testes devem ser chamados pela ordem dos nomes
// dos m�todos
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginModo2Test {

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	// Classe respons�vel por conter os testes da p�gina de login da aplica��o a ser
	// testada
	LoginPageObject loginPageObject = new LoginPageObject();

	private String metodoNome;

	// O m�todo after � chamado ap�s cada m�todo de @Test
	@After
	public void capiturarTelaDepoisDoTeste() {
		// Com essa chamada capturamos a tela que estamos testando
		// metodonome � o nome do arquivo que estamos criando, que � atualizado a cada
		// chamada dos m�todos de testes.
		ScreenshotUtil.printar("LoginModo2Page\\", metodoNome);
	}

	// Para que a suite entenda que o m�todo deve ser chamado ele tem que ser
	// anotado com @Test assim como JUnit, sem essa anota��o esse m�todo n�o ser�
	// chamado.
	@Test
	public void A001AcessarMenuNovoUsuario() {
		// Aqui pegamos o nome do m�todo que estamos testando
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageObject.acessarMenuLogin();
	}

	@Test
	public void A002InformarEmail() {
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageObject.informarEmail(properties.getEmail());
	}

	@Test
	public void A003InformarSenha() {
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageObject.informarSenha(properties.getSenha());
	}

	@Test
	public void A004Cadastrar() {
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageObject.clicarBotaoEntrar();
	}

	// Se n�o houver valida��es, estamos apenas criando um rob� que executa os
	// passos do usu�rio, por isso � necess�rio criar os asserts, s�o eles que
	// ir�o validar se a informa��o encontrada est� conforme a esperada.
	@Test
	public void A005ValidarCadastro() {
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageObject.isLogado();
	}

	@Test
	public void A006Sair() {
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageObject.sair();
	}
}
