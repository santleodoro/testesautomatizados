package treinamento.tests;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import treinamento.pages.LoginPageObject;
import treinamento.util.LeitorProperties;

/**
 * Classe respons�vel por executar os tests nas telas da aplica��o.
 * 
 * Aqui os testes est�o sendo chamados com um @Test para cada intera��o, prefiro
 * dessa forma, pois se houver erro de elemnto n�o encontrado em algum campo,
 * ser� poss�vel saber qual campo est� com problema.
 * 
 * @author Sandra Rodrigues
 *
 */
// Anota��o para informar que os testes devem ser chamados pela ordem dos nomes
// dos m�todos
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginModo1Test {

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	// Classe respons�vel por conter os testes da p�gina de login da aplica��o a ser
	// testada
	LoginPageObject loginPageObject = new LoginPageObject();

	// Para que a suite entenda que o m�todo deve ser chamado ele tem que ser
	// anotado com @Test assim como JUnit, sem essa anota��o esse m�todo n�o ser�
	// chamado.
	@Test
	public void A000Logar() {
		loginPageObject.acessarMenuLogin().informarEmail(properties.getEmail()).informarSenha(properties.getSenha())
				.clicarBotaoEntrar().isLogado().sair();
	}

}
