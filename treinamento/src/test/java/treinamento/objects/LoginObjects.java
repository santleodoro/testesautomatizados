package treinamento.objects;

import org.openqa.selenium.By;

/**
 * Classe que cont�m os mapeamentos dos campos da tela
 * 
 * @author Sandra Rodrigues
 *
 */
public class LoginObjects extends CommonObjects {

	public static By elementoMenuLogin = By.linkText("Login");

	// O Xpath pode ser usado, desde que saiba utilizar, veja o exemplo abaixo:
	// Se for para utilizar o xpath da maneira errada ele ficaria assim:
	// public static By elementoBotaoEntrar =
	// By.xpath("//html/body/div[2]/form/button");
	// Repare que ele percorre pelo corpo do DOM todo, passa por todas as tags,
	// performarticamente falando isso � muito ruim.
	// A melhor maneira de utilizar seria eliminando campos que n�o s�o necess�rios
	// Primeiro informar // (duas barras) significa que foi iniciada uma busca, em
	// seguida informar o nome da propriedade do elemento que est� buscando, nessa
	// caso � um button, depois informar que nesse button existe um texto "Entrar",
	// dessa forma o xpath fica claro de ser lido, de realizar manuten��o, e
	// otimizado.
	public static By elementoBotaoEntrar = By.xpath("//button[text()='Entrar']");
	public static By elementoOpcaoSair = By.linkText("Sair");
}
