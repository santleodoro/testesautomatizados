package treinamento.objects;

import org.openqa.selenium.By;

/**
 * Classe responsável por conter o mapeamento de elementos comuns as todas as
 * classes de teste.
 * 
 * @author Sandra Rodrigues
 *
 */
public class CommonObjects {
	public static By resultado = By.xpath("//div[@role='alert']");
	public static By elementoCampoEmail = By.id("email");
	public static By elementoCampoSenha = By.id("senha");
}
