package treinamento.objects;

import org.openqa.selenium.By;

/**
 * Classe que cont�m os mapeamentos dos campos da tela
 * 
 * @author Sandra Rodrigues
 *
 */
public class CadastroObjects extends CommonObjects {

	public static By elementoMenuNovoUsuario = By.linkText("Novo usu�rio?");
	public static By elementoCampoNome = By.id("nome");

	// O Xpath pode ser usado, desde que saiba utilizar, veja o exemplo abaixo:
	// Se for para utilizar o xpath da maneira errada ele ficaria assim:
	// public static By elementoBotaoEntrar =
	// By.xpath("//html/body/div[2]/form/input");
	// Repare que ele percorre pelo corpo do DOM todo, passa por todas as tags,
	// performarticamente falando isso � muito ruim.
	// A melhor maneira de utilizar seria eliminando campos que n�o s�o necess�rios
	// Primeiro informar // (duas barras) significa que foi iniciada uma busca, em
	// seguida informar o nome da propriedade do elemento que est� buscando, nessa
	// caso � um input, depois informar que nesse input existe uma propriedade,
	// value no caso, (as propriedades dentro de uma tag, para se encontrada devem
	// vir precedidas de @) que contem o texto Cadastrar, dessa forma o xpath fica
	// claro de ser lido, de realizar manuten��o, e otimizado.
	public static By elementoBotaoCadastrar = By.xpath("//input[@value='Cadastrar']");
}
