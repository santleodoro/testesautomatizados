package treinamento.pages;

import static org.junit.Assert.assertEquals;

import treinamento.objects.CadastroObjects;

/**
 * Classe respons�vel por reproduzir os passos do usu�rio e ou testador na tela.
 * 
 * @author Sandra Rodrigues
 *
 */
public class CadastroPageObject {

	// A BasePage possui m�todos comuns de manipula��o das telas, como clique,
	// inser��o de textos, esperas, etc, pode ser chamada de qualquer classe de
	// PageObjects.
	BasePageObjects basePage = new BasePageObjects();

	public CadastroPageObject() {
	}

	// A cada passo retornamos a pageobject para que ela consiga chamar o pr�ximo
	// passo com mais facilidade
	public CadastroPageObject acessarMenuNovoUsario() {
		basePage.clicar(CadastroObjects.elementoMenuNovoUsuario);
		return this;
	}

	public CadastroPageObject informarNome(String nome) {
		basePage.informarTexto(CadastroObjects.elementoCampoNome, nome);
		return this;
	}

	public CadastroPageObject informarEmail(String email) {
		basePage.informarTexto(CadastroObjects.elementoCampoEmail, email);
		return this;
	}

	public CadastroPageObject informarSenha(String senha) {
		basePage.informarTexto(CadastroObjects.elementoCampoSenha, senha);
		return this;
	}

	public CadastroPageObject clicarBotaoCadastrar() {
		basePage.clicar(CadastroObjects.elementoBotaoCadastrar);
		return this;
	}

	public CadastroPageObject isUsuarioCadastrado() {
		// O assert, do JUnit, n�o use do framewor, � o respons�vel por comparar o valor
		// procurado com o valor encontrado. Valida os testes executados.
		assertEquals("Usu�rio inserido com sucesso", basePage.getTextoDoElemento(CadastroObjects.resultado));
		return this;
	}
}
