package treinamento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import treinamento.util.DriverUtil;

public class BasePageObjects {

	public BasePageObjects() {
	}

	/**
	 * M�todo respons�vel por retornar o texto do campo informado no paramentro.
	 * 
	 * @param elemento
	 */
	public String getTextoDoElemento(By elemento) {
		// O campo da tela com o qual ocorrer� a intera��o
		WebElement campo = getCampo(elemento);
		// retornar o texto contido no campo encontrado
		return campo.getText();
	}

	/**
	 * M�todo respons�vel por clicar no campo informado no paramentro.
	 * 
	 * @param elemento
	 */
	public void clicar(By elemento) {
		// O campo da tela com o qual ocorrer� a intera��o
		WebElement campo = getCampo(elemento);
		// Clicar no campo encontrado
		campo.click();
	}

	/**
	 * M�todo respons�vel por esperar o elemento ser carregado, procurar o campo
	 * referente a esse elemento na tela, e retornar o campo encontrado
	 * 
	 * @param elemento
	 * @return
	 */
	private WebElement getCampo(By elemento) {
		// Aguardar o elemento ser encontrado
		DriverUtil.aguardar(elemento);

		// Procura o elemento na tela e retorna o campo referente a esse elemento
		return DriverUtil.findElement(elemento);
	}

	/**
	 * M�todo respons�vel por colocar o texto do parametro texto dentro do campo
	 * informado no paramentro elemento.
	 * 
	 * @param elemento
	 */
	public void informarTexto(By elemento, String texto) {
		// O campo da tela com o qual ocorrer� a intera��o
		WebElement campo = getCampo(elemento);
		// Por boa pr�tica deve-se limpar o campo antes de interagir com ele, para ter
		// certeza que n�o h� nenhum valor no campo antes de informar um novo valor.
		campo.clear();
		// Enviar o valor para dentro do campo
		campo.sendKeys(texto);
	}

}