package treinamento.pages;

import static org.junit.Assert.assertEquals;

import treinamento.objects.LoginObjects;
import treinamento.util.LeitorProperties;

/**
 * Classe respons�vel por reproduzir os passos do usu�rio e ou testador na tela.
 * 
 * @author Sandra Rodrigues
 *
 */
public class LoginPageObject {

	// A BasePage possui m�todos comuns de manipula��o das telas, como clique,
	// inser��o de textos, esperas, etc, pode ser chamada de qualquer classe de
	// PageObjects.
	BasePageObjects basePage = new BasePageObjects();

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	public LoginPageObject() {
	}

	// A cada passo retornamos a pageobject para que ela consiga chamar o pr�ximo
	// passo com mais facilidade
	public LoginPageObject acessarMenuLogin() {
		basePage.clicar(LoginObjects.elementoMenuLogin);
		return this;
	}

	public LoginPageObject informarEmail(String email) {
		basePage.informarTexto(LoginObjects.elementoCampoEmail, email);
		return this;
	}

	public LoginPageObject informarSenha(String senha) {
		basePage.informarTexto(LoginObjects.elementoCampoSenha, senha);
		return this;
	}

	public LoginPageObject clicarBotaoEntrar() {
		basePage.clicar(LoginObjects.elementoBotaoEntrar);
		return this;
	}

	public LoginPageObject sair() {
		basePage.clicar(LoginObjects.elementoOpcaoSair);
		return this;
	}

	// O assert, do JUnit, n�o use do framewor, � o respons�vel por comparar o valor
	// procurado com o valor encontrado. Valida os testes executados.
	public LoginPageObject isLogado() {
		assertEquals("Bem vindo, ".concat(properties.getNome()).concat("!"),
				basePage.getTextoDoElemento(LoginObjects.resultado));
		return this;
	}
}
