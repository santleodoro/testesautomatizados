package treinamento.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Classe que vai retornar os dados do arquivo dados.propertie
 * 
 * @author Sandra Rodrigues
 *
 */
public class LeitorProperties extends ArquivoDiretorioUtil {

	private String nome;
	private String email;
	private String senha;
	Properties props = null;

	public Properties getProp() {
		props = new Properties();
		try {
			FileInputStream file = new FileInputStream(dirArquivoProperties + "dados.properties");
			props.load(file);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return props;
	}

	public LeitorProperties() {
		if (props == null) {
			props = getProp();
		}
		nome = props.getProperty("prop.usuario.nome");
		email = props.getProperty("prop.usuario.email");
		senha = props.getProperty("prop.usuario.senha");
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
