package treinamento.util;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;

/**
 * Classe respons�vel por tirar os prints das telas.
 * 
 * @author Sandra Rodrigues
 *
 */
public class ScreenshotUtil {

	public static void printar(String pasta, String arquivo) {
		// Informa o nome do arquivo
		String nomeArquivo = DriverUtil.dirRelatorios.concat(pasta).concat(getNomeArquivo(arquivo));

		// Faz o print
		File screenshot = DriverUtil.getTakeScreenshot().getScreenshotAs(OutputType.FILE);
		try {
			// Pega o print e coloca na pasta e nome do arquivo informado no parametro do
			// m�todo.
			FileUtils.copyFile(screenshot, new File(nomeArquivo));
		} catch (Exception e) {
			// Caso tenha ocorrido algum erro, n�o � problema dos testes, apenas mostrar o
			// erro no console mas n�o sobe a exception para n�o atrapalhar os testes.
			System.out.println("Houve um problema na execu��o do Print: " + e.getMessage());
		}
	}

	private static String getNomeArquivo(String metodoNome) {
		// Monta o nome do arquivo do print
		return getDataHora().concat(" ").concat(metodoNome).concat(".png");
	}

	public static String getDataHora() {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		return new SimpleDateFormat("yyyyMMdd_HHmm").format(ts);
	}
}
