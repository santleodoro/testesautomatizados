package treinamento.util;

import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Classe respons�vel por executar as configura��es do driver.
 * 
 * @author Sandra Rodrigues
 *
 */
public class DriverUtil extends ArquivoDiretorioUtil {
	private static WebDriver driver;
	private static WebDriverWait wait;

	public static TakesScreenshot getTakeScreenshot() {
		return ((TakesScreenshot) DriverUtil.driver);
	}

	public static void configuraDriver() {
		// Informando ao driver onde se ecnontra o arquivo execut�vel
		System.setProperty("webdriver.chrome.driver", driverFile);

		// Auxilia para realizar algumas intera��es com o navegador, como por exemplo,
		// j� abrir a tela maximizada
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");

		// Instancia o driver, que � o respons�vel por realizar as intera��es com o
		// navegador
		driver = new ChromeDriver(options);

		// Respons�vel por esperar um tempo para que o elemento tenha tempo de ser
		// carregado no navegador, nesse caso colocamos 30 segundos, caso o elemento
		// seja encontrado antes de 30 segundos os testes seguem normalmente,por�m, se
		// depois de 30 segundos n�o for encontrado mostra um erro similar ao abaixo:
		// "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for
		// visibility of element located by By.id: nomeElemento (tried for 30 second(s)
		// with
		// 500 milliseconds interval)"
		wait = new WebDriverWait(DriverUtil.driver, 30);
	}

	public static void abrirUrl(String url) {
		driver.get(url);
	}

	public static void fecharNavegador() {
		driver.quit();
	}

	public static void aguardar(By elemento) {
		DriverUtil.wait.until(ExpectedConditions.visibilityOfElementLocated(elemento));
	}

	public static WebElement findElement(By elemento) {
		return DriverUtil.driver.findElement(elemento);
	}
}
