package treinamento.suites;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import treinamento.tests.CadastroModo1Test;
import treinamento.tests.CadastroModo2Test;
import treinamento.tests.LoginModo1Test;
import treinamento.tests.LoginModo2Test;
import treinamento.util.DriverUtil;

//Responsável por gerenciar as classes de testes que serão rodadas
@RunWith(Suite.class)
// Chamada das classes de teste
@SuiteClasses({

		CadastroModo1Test.class,

		LoginModo1Test.class,

		CadastroModo2Test.class,

		LoginModo2Test.class

})

public class SuitePrincipal {
	// Site que iremos acessar
	private static String url = "https://srbarriga.herokuapp.com/login";

	// Método com essa anotação informa que deve ocorrer antes de tudo que for
	// responsábilidade dessa classe
	@BeforeClass
	public static void setUp() {
		DriverUtil.configuraDriver();
		// Abre a URL informada
		DriverUtil.abrirUrl(url);
	}

	// Método com essa anotação informa que deve ocorrer depois de tudo que for
	// responsábilidade dessa classe
	@AfterClass
	public static void tearDown() {
		// Responsável por fechar o navegador depois que os testes terminam
		DriverUtil.fecharNavegador();
	}
}
